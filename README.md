# cloudformation-aws-scp-notification

An AWS CloudFormation template to deploy an application that notifies SNS Subscriptions for Organizations Service Control Policy (SCP) changes.

## Resources

- EventBridge Rule
- SNS Topic and Policy

## Prerequisites

- An AWS organization with SCPs deployed to manage linked accounts.
- This solution should be deployed in the Master account.

## Deployment Steps

### Quick Link

Simply login to your AWS account and click on this [quick link](https://console.aws.amazon.com/cloudformation/home?#/stacks/create/review?templateURL=https://warpedlenses-public.s3.ap-southeast-1.amazonaws.com/cloudformation/notifyme.yaml).

### Manual Template Upload

- Deploy this solution in the CloudFormation console.
    - Only a name / title is needed for the parameters.
- Once the deployment is complete, go to the SNS console and subscribe to get notified if any SCP is changed.
